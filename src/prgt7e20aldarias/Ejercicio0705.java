/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package prgt7e20aldarias;

/**
 * Fichero: Ejercicio0705.java
 * @date 28-ene-2014
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 */
public class Ejercicio0705 {

private static int[] lista;
  final static int POS=10; //numero de posiciones del array
  final static int LIMITE=10; //Numeros entre 1..Límite
  public static int getaleatorio() {
    return (int) (Math.random()*LIMITE+1);
  }
  public static void ordena(int array[]) {
    int aux;
    for (int i=array.length; i>0; i--) {
      for (int j=0; j<i-1; j++) {
        if (array[j]>array[j+1]) {
          aux = array[j+1];
          array[j+1]=array[j];
          array[j]=aux;
        }
      }
    }
  }
  public static void muestra() {
    for (int i=0; i<POS; i++) {
      System.out.print(lista[i]+" ");
    }
  }
  public static boolean encuentra(int num) {
    for (int i=0; i<POS; i++) {
      if (lista[i]==num) {
        return true;
      }
      if (lista[i]>num) {
        return false;
      }
    }
    return false;
  }
  public static void main(String[] args) {
    lista = new int[POS];
    for (int i=0; i<POS; i++) {
      lista[i]=getaleatorio();
    }
    muestra();//se muestra el vector desordenado
    System.out.println("");
    ordena(lista); //ordenacion por burbuja
    muestra(); //se muestra el vector ordenado
    System.out.println("");
    
    System.out.println("No esta en el array los numeros: ");
    for (int i=1; i<=POS; i++) {
      if (!encuentra(i)) {
        System.out.print(i+" ");
      }
    }
    System.out.println("");
  }
}

/* EJECUCION:
2 8 8 10 9 3 5 4 8 5 
2 3 4 5 5 8 8 8 9 10 
No esta en el array los numeros: 
1 6 7 
*/
