/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package prgt7e20aldarias;

/**
 * Fichero: Ejercicio0703.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 17-ene-2014
 */
public class Ejercicio0703 {

  final static int POS = 50; //número de posiciones del array
  final static int LIMITE = 100; //Números entre 1..Límite
  //***

  public static int getaleatorio() {
    return (int) (Math.random() * LIMITE + 1);
  }

  // ***
  public static int[] combina(int[] lista1, int[] lista2) {
    int[] lista = new int[lista1.length + lista2.length];
    System.arraycopy(lista1, 0, lista, 0, lista1.length);
    System.arraycopy(lista1, 0, lista, lista1.length, lista2.length);
    return lista;
  }

  //*** otra forma
  public static int[] combina1(int[] lista1, int[] lista2) {
    int[] l = new int[lista1.length + lista2.length];
    int i = 0;
    for (int j = 0; j < lista1.length; j++) {
      l[i] = lista1[j];
      i++;
    }
    for (int j = 0; j < lista2.length; j++) {
      l[i] = lista2[j];
      i++;
    }

    return l;
  }
  //***

  public static void ordena(int array[]) {
    int aux;
    boolean cambio;
    for (int i = array.length; i > 0; i--) {
      cambio = false;
      for (int j = 0; j < i - 1; j++) {
        if (array[j] > array[j + 1]) {
          aux = array[j + 1];
          array[j + 1] = array[j];
          array[j] = aux;
          cambio = true;
        }
      }
      if (!cambio) {
        return;
      }
    }
  }
  //***

  public static void muestra(int array[]) {
    for (int i = 0; i < array.length; i++) {
      System.out.print(array[i] + " ");
    }
  }

  public static void main(String[] args) {
    int[] lista1 = new int[POS];
    int[] lista2 = new int[POS];
    int[] ltot = new int[2 * POS];
    for (int i = 0; i < POS; i++) {
      lista1[i] = getaleatorio();
      lista2[i] = getaleatorio();
    }
    System.out.println("");
    ltot = combina(lista1, lista2);
    ordena(ltot);
    muestra(ltot);//se muestra el vector ordenado
    System.out.println("");
  }
}

/* EJECUCION:
3 5 6 6 7 7 9 10 10 11 13 14 14 16 17 18 18 18 19 19 19 22 25 26 29 32 
33 33 33 34 34 35 37 38 39 39 39 39 39 40 43 44 48 48 48 49 49 49 49 
50 52 52 53 58 59 60 61 62 63 63 63 64 65 68 68 68 69 70 70 70 71 71 
72 72 73 74 75 75 76 77 80 81 83 83 83 84 84 84 88 89 89 90 90 91 92 
92 94 94 98 99
*/
